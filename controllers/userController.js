const User = require("../models/user");
const { findUserById } = require("../utils/utils");

module.exports.getUser = async (request, response) => {
  const { _id: currentUserId } = request.user;
  try {
    const user = await findUserById(currentUserId);
    const { _id, username, createdDate } = user;
    response.json({
      user: {
        _id,
        username,
        createdDate,
      },
    });
  } catch (error) {
    if (error.status !== undefined) {
      response.status(error.status).json({ message: error.message });
    } else {
      response.status(500).json({ message: error.message });
    }
  }
};

module.exports.deleteUser = async (request, response) => {
  const { _id: currentUserId } = request.user;
  try {
    const user = await findUserById(currentUserId);
    await User.deleteOne(user);
    response.json({
      message: "success",
    });
  } catch (error) {
    if (error.status !== undefined) {
      response.status(error.status).json({ message: error.message });
    } else {
      response.status(500).json({ message: error.message });
    }
  }
};

module.exports.changeUserPassword = async (request, response) => {
  const { _id: currentUserId } = request.user;
  const { oldPassword, newPassword } = request.body;
  try {
    if (!oldPassword || !newPassword) {
      throw {
        status: 400,
        message: "Paths `oldPassword` and `newPassword` are required",
      };
    }
    const user = await findUserById(currentUserId);
    if (user.password !== oldPassword) {
      throw {
        status: 403,
        message: "Old password is not equal to the current one",
      };
    }
    await User.updateOne(user, { password: newPassword });
    response.json({
      message: "success",
    });
  } catch (error) {
    if (error.status !== undefined) {
      response.status(error.status).json({ message: error.message });
    } else {
      response.status(500).json({ message: error.message });
    }
  }
};
