const Note = require("../models/note");
const { findNoteById } = require("../utils/utils");

module.exports.getNoteById = async (request, response, next) => {
  const { id: noteId } = request.params;
  const { _id: currentUserId } = request.user;
  if (noteId) {
    try {
      const { _id, userId, completed, text, createdDate } = await findNoteById(
        noteId,
        currentUserId
      );
      response.json({
        note: {
          _id,
          userId,
          completed,
          text,
          createdDate,
        },
      });
    } catch (error) {
      if (error.status !== undefined) {
        response.status(error.status).json({ message: error.message });
      } else {
        response.status(500).json({ message: error.message });
      }
    }
  } else {
    next();
  }
};

module.exports.getAllUserNotes = async (request, response) => {
  const { _id: currentUserId } = request.user;
  try {
    const notes = await Note.find({ userId: currentUserId });
    // if (notes.length === 0) {
    //   throw { status: 400, message: "User has no notes" };
    // }
    response.json({ notes });
  } catch (error) {
    if (error.status !== undefined) {
      response.status(error.status).json({ message: error.message });
    } else {
      response.status(500).json({ message: error.message });
    }
  }
};

module.exports.addNote = async (request, response) => {
  const { text } = request.body;
  const { _id } = request.user;
  try {
    if (!text) {
      throw {
        status: 400,
        message: "Path `text` is required",
      };
    }
    const note = new Note({
      userId: _id,
      completed: false,
      text,
      createdDate: new Date().toISOString(),
    });
    await note.save();
    response.json({ message: "success" });
  } catch (error) {
    if (error.status !== undefined) {
      response.status(error.status).json({ message: error.message });
    } else {
      response.status(500).json({ message: error.message });
    }
  }
};

module.exports.updateNoteById = async (request, response) => {
  const { id: noteId } = request.params;
  const { _id: currentUserId } = request.user;
  const { text } = request.body;
  try {
    if (!text) {
      throw {
        status: 400,
        message: "Path `text` is required",
      };
    }
    const note = await findNoteById(noteId, currentUserId);
    await Note.updateOne(note, { text });
    response.json({
      message: "success",
    });
  } catch (error) {
    if (error.status !== undefined) {
      response.status(error.status).json({ message: error.message });
    } else {
      response.status(500).json({ message: error.message });
    }
  }
};

module.exports.checkById = async (request, response) => {
  const { id: noteId } = request.params;
  const { _id: currentUserId } = request.user;
  try {
    const note = await findNoteById(noteId, currentUserId);
    await Note.updateOne(note, { completed: !note.completed });
    response.json({
      message: "success",
    });
  } catch (error) {
    if (error.status !== undefined) {
      response.status(error.status).json({ message: error.message });
    } else {
      response.status(500).json({ message: error.message });
    }
  }
};

module.exports.deleteNote = async (request, response) => {
  const { id: noteId } = request.params;
  const { _id: currentUserId } = request.user;
  try {
    const note = await findNoteById(noteId, currentUserId);
    await Note.deleteOne(note);
    response.json({
      message: "success",
    });
  } catch (error) {
    if (error.status !== undefined) {
      response.status(error.status).json({ message: error.message });
    } else {
      response.status(500).json({ message: error.message });
    }
  }
};
