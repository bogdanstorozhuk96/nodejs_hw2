const jwt = require("jsonwebtoken");

const User = require("../models/user");

const { secret } = require("../config/auth");

module.exports.register = async (request, response) => {
  const { username, password } = request.body;
  try {
    if (!username || !password) {
      throw {
        status: 400,
        message: "Path `username` and `password` is required",
      };
    }
    const user = new User({
      username,
      password,
      createdDate: new Date().toISOString(),
    });
    await user.save();
    response.json({ message: "success" });
  } catch (error) {
    if (error.status !== undefined) {
      response.status(error.status).json({ message: error.message });
    } else {
      response.status(500).json({ message: error.message });
    }
  }
};

module.exports.login = async (request, response) => {
  const { username, password } = request.body;
  try {
    if (!username || !password) {
      throw {
        status: 400,
        message: "Path `username` and `password` is required",
      };
    }
    const user = await User.findOne({ username, password });
    if (!user) {
      throw {
        status: 400,
        message: "No user with such name and password found",
      };
    }
    response.json({
      jwt_token: jwt.sign(JSON.stringify(user), secret),
    });
  } catch (error) {
    if (error.status !== undefined) {
      response.status(error.status).json({ message: error.message });
    } else {
      response.status(500).json({ message: error.message });
    }
  }
};
