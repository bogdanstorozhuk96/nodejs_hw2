const Note = require("../models/note");
const User = require("../models/user");

module.exports.findNoteById = async (noteId, currentUserId) => {
  const note = await Note.findById(noteId);
  if (!note) {
    throw { status: 400, message: "No note with such id found" };
  }
  if (note.userId !== currentUserId) {
    throw { status: 403, message: "You don't have access to this note" };
  }
  return note;
};

module.exports.findUserById = async (currentUserId) => {
  const user = await User.findById(currentUserId);
  if (!user) {
    throw { status: 400, message: "No user with such id found" };
  }
  return user;
};
