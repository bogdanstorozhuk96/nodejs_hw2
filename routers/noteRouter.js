const express = require("express");
const router = express.Router();

const {
  addNote,
  deleteNote,
  updateNoteById,
  checkById,
  getNoteById,
  getAllUserNotes,
} = require("../controllers/noteContoller");

const authMiddleware = require("../middlewares/authMiddleware");

router.post("/notes", authMiddleware, addNote);
router.delete("/notes/:id", authMiddleware, deleteNote);
router.put("/notes/:id", authMiddleware, updateNoteById);
router.patch("/notes/:id", authMiddleware, checkById);
router.get("/notes/:id", authMiddleware, getNoteById);
router.get("/notes", authMiddleware, getAllUserNotes);
module.exports = router;
