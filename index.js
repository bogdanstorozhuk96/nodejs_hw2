const mongoose = require("mongoose");
const express = require("express");
const app = express();
const cors = require("cors");

const { port } = require("./config/server");
const { dbUserUsername, dbUserPassword } = require("./config/database");

const userRouter = require("./routers/userRouter");
const authRouter = require("./routers/authRouter");
const noteRouter = require("./routers/noteRouter");

const uri = `mongodb+srv://dbUser:${dbUserPassword}@cluster0.bnkxk.mongodb.net/${dbUserUsername}?retryWrites=true&w=majority`;

mongoose.connect(uri, {
  useNewUrlParser: true,
  useUnifiedTopology: true,
  useFindAndModify: false,
  useCreateIndex: true,
});

app.use(cors());

app.use(express.json());

app.use("/api", userRouter);
app.use("/api", authRouter);
app.use("/api", noteRouter);

app.listen(port, () => {
  console.log(`Server started on ${port} port`);
});
